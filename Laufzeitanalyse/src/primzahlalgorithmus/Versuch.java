package primzahlalgorithmus;


public class Versuch {

	private long[] testZahlen;
	private StoppUhr testUhr;
	private long[] ergebnisZeiten;

	public Versuch(long[] testZahlen) {
		super();
		this.testZahlen = testZahlen;
		this.testUhr = new StoppUhr();
	}

	public Versuch() {
		super();
		this.testZahlen = new long[20];
		for (int i = 0; i < testZahlen.length; i++) {
			testZahlen[i] = (long) (Math.random() * 10000000);
		}
		this.testUhr = new StoppUhr();
	}

	public void durchfuehrung() {
		isPrim(1);
		this.ergebnisZeiten = new long[this.testZahlen.length];
		for (int i = 0; i < testZahlen.length; i++) {
			testUhr.start();
			isPrim(testZahlen[i]);
			testUhr.stopp();
			ergebnisZeiten[i] = testUhr.getTimeMili();
		}
	}

	public long[] getErgebnisZeiten() {
		return ergebnisZeiten;
	}

	public static boolean isPrim(long zahl) {
		boolean isPrim = true;
		Math.abs(zahl);
		if (zahl <= 1)
			isPrim = false;
		for (long i = 2; i < zahl; i++)
			if (zahl % i == 0)
				isPrim = false;
		return isPrim;
	}

}
