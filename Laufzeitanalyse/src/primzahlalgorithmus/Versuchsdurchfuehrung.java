package primzahlalgorithmus;

import java.util.Arrays;

public class Versuchsdurchfuehrung {

	public static void main(String[] args) {
		long[] n = { 81866077L, 124824792L, 54279483L, 37885427L, 39481418L, 47392124L, 938395162L, 130152827L,
				42958252L, 30294258L };
		Arrays.sort(n);
		Versuch versuch = new Versuch(n);

		versuch.durchfuehrung();

		long[] t = versuch.getErgebnisZeiten();

		System.out.println("t(n) = ");
		for (int i = 0; i < n.length; i++) {
			System.out.println("t(" + n[i] + ")\t= " + t[i] + "ms");
		}
	}

}
