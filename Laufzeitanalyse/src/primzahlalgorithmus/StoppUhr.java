package primzahlalgorithmus;

public class StoppUhr {
	private long startZeit;
	private long endZeit;

	public StoppUhr() {
		super();
		this.startZeit = 0;
		this.endZeit = 0;
	}

	public void start() {
		this.startZeit = System.currentTimeMillis();
		this.endZeit = 0;
	}

	public void stopp() {
		this.endZeit = System.currentTimeMillis();
	}

	public long getTimeMili() {
		return endZeit - startZeit;
	}

}
