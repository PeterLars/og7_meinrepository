package bubblesort;

public class Bubblesort {
	public static void bubblesort(int[] zahlen) {
		for (int i = 0; i < zahlen.length; i++) {
			for (int j = 0; j < zahlen.length-1; j++) {
				if (zahlen[j] > zahlen[j+1]) {
					int temp = zahlen[j];
					zahlen[j] = zahlen[j+1];
					zahlen[j+1] = temp;
				}
			}
		}
		
	}
	
	public static void bubblesortOptimiert(int[] zahlen) {
		for (int i = 0; i < zahlen.length; i++) {
			for (int j = 0; j < zahlen.length-i-1; j++) {
				if (zahlen[j] > zahlen[j+1]) {
					int temp = zahlen[j];
					zahlen[j] = zahlen[j+1];
					zahlen[j+1] = temp;
				}
			}
		}
		
	}
}
