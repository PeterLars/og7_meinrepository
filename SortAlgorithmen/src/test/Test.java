package test;

import bubblesort.Bubblesort;

public class Test {

	public static void main(String[] args) {
		
		int[] zahlen = new int[30];
		for (int i = 0; i < zahlen.length; i++) {
			zahlen[i] = (int) (Math.random()*100);
		}
		
		long start = System.nanoTime();
		Bubblesort.bubblesortOptimiert(zahlen);
		Bubblesort.bubblesort(zahlen);
		long end = System.nanoTime();
		System.out.println((end-start) + " Nanos\n");
		
		for (int i : zahlen) {
		System.out.println(i);
		}
		
	}

}
