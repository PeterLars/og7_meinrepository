package rekursiv;

import java.util.Arrays;

public class test {

	public static void main(String[] args) {
		StoppUhr stoppUhr = new StoppUhr();
		int x = 600000000;
		int[] A = new int[2000000];
		int aR = (int) (Math.random() * A.length);
		for (int i = 0; i < A.length; i++) {
			if (i == aR)
				A[i] = x;
			else
				A[i] = (int) (Math.random() * A.length * 1);
		}

		search(x, A);
		stoppUhr.start();
		int a = search(x, A);
		stoppUhr.stopp();
		if (a != -1) {
			System.out.println("Stelle: " + a + "\nGesuchte Zahl: " + x + "\nGefundene Zahl: " + A[a] + "\nEs hat "
					+ stoppUhr.getTimeMili() + "ms gebraucht diese Zahl zu finden");
		} else {
			System.out.println("Die Zahl wurde nicht gefunden" + "\nEs hat " + stoppUhr.getTimeMili()
					+ "ms gebraucht diese Zahl nicht zu finden");
		}

		fastsearch(x, A);
		System.out.println("");
		stoppUhr.start();
		int b = fastsearch(x, A);
		stoppUhr.stopp();
		if (b != -1) {
			System.out.println("Stelle: " + b + "\nGesuchte Zahl: " + x + "\nGefundene Zahl: " + A[b] + "\nEs hat "
					+ stoppUhr.getTimeMili() + "ms gebraucht diese Zahl zu finden");
		} else {
			System.out.println("Die Zahl wurde nicht gefunden" + "\nEs hat " + stoppUhr.getTimeMili()
					+ "ms gebraucht diese Zahl nicht zu finden");
		}
	}

	public static int search(int x, int[] A) {
		for (int i = 0; i < A.length; i++)
			if (A[i] == x)
				return i;
		return -1;
	}

	public static int fastsearch(int x, int[] A) {
		Arrays.sort(A);
		return fastsearchR(0, A.length - 1, x, A);
	}

	public static int fastsearchR(int von, int bis, int x, int[] A) {
		if (von <= bis) {
			int mB = (von + bis) / 2;
			int mI = von + (bis - von) * ((x - A[von]) / (A[bis] - A[von]));

			if (mI >= A.length)
				mI -= A.length;
			if (mI < 0)
				Math.abs(mI);
			if (mB > mI) {
				int temp = mB;
				mB = mI;
				mI = temp;
			}
			if (x == A[mB])
				return mB;
			if (x == A[mI])
				return mI;
			if (x < A[mB])
				return fastsearchR(von, mB - 1, x, A);
			if (x < A[mI])
				return fastsearchR(mB + 1, mI - 1, x, A);
			return fastsearchR(mI + 1, bis, x, A);
		}
		return -1;
	}

}
