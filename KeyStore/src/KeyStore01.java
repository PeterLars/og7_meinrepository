
public class KeyStore01 {
	// Attribute
	private String[] keys;

	// Constructor
	public KeyStore01() {
		super();
		this.keys = new String[100];
	}

	public KeyStore01(int length) {
		super();
		this.keys = new String[length];
	}

	// Methodes
	public boolean add(String e) {
		int i = size();
		if (i != -1) {
			keys[i] = e;
			return true;
		}
		return false;

	}

	public String get(int index) {
		if (index < size() && index >= 0)
			return keys[index];
		return "";
	}

	public int indexOf(String str) {
		for (int i = 0; i < size(); i++) {
			if (get(i).equals(str))
				return i;
		}
		return -1;
	}

	public int size() {
		for (int i = 0; i < keys.length; i++) {
			if (keys[i] == null) {
				return i;
			}
		}
		return -1;
	}

	public boolean remove(int index) {
		if (index < size() && index >= 0) {
			for (int i = index + 1; i < size(); i++) {
				keys[i - 1] = get(i);
			}
			return true;
		} else {
			return false;
		}
	}

	public boolean remove(String str) {
		return remove(indexOf(str));
	}

	public void clear() {
		for (int i = 0; i < keys.length; i++) {
			keys[i] = null;
		}
	}

	@Override
	public String toString() {
		String ausgabe = "[";
		for (int i = 0; i < size(); i++) {
			if (i <= 0)
				ausgabe += get(i);
			else {
				ausgabe += ", " + get(i);
			}
		}
		ausgabe += "]";
		return ausgabe;
	}
}
