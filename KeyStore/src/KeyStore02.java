import java.util.ArrayList;

public class KeyStore02 {
	// Attribute
	private ArrayList<String> keys;

	// Constructor
	public KeyStore02() {
		super();
		this.keys = new ArrayList<>();
	}

	public KeyStore02(int length) {
		super();
		this.keys = new ArrayList<>(length);
	}

	// Methodes
	public boolean add(String e) {
		return keys.add(e);
	}

	public String get(int index) {
		return keys.get(index);
	}

	public int indexOf(String str) {
		return keys.indexOf(str);
	}

	public int size() {
		return keys.size();
	}

	public boolean remove(int index) {
		return keys.remove(index) != null;
	}

	public boolean remove(String str) {
		return keys.remove(str);
	}

	public void clear() {
		keys.clear();
	}

	@Override
	public String toString() {
		return keys.toString();
	}
}
