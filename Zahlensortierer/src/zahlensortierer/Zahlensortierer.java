package zahlensortierer;

import java.util.*;

//TODO add your name as author
/**
 * @author Tim Tenbusch
 *
 */
public class Zahlensortierer {

	// TODO
	// vervollst�ndigen Sie die main methode so, dass sie 3 Zahlen vom Benutzer
	// einliest und die kleinste und gr��te Zahl ausgibt.
	public static void main(String[] args) {

		Scanner myScanner = new Scanner(System.in);
		
		int[] zahlen = new int[3];
		for (int i = 0; i < zahlen.length; i++) {
			System.out.print((i+1) + ". Zahl: ");
			try {
				zahlen[i] = Integer.parseInt(myScanner.nextLine());
			} catch (Exception e) {
				i--;
				System.out.print("");
				System.out.println("\tBitte geben Sie eine Zahl ein!");
			}
			
		}

		System.out.println();
		
		Arrays.sort(zahlen);
		
		System.out.println("Kleinste Zahl: " + zahlen[0]);
		System.out.println("Gr��te Zahl  : " + zahlen[zahlen.length-1]);
		

		myScanner.close();
	}
}