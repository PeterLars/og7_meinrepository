package zooTycoon;

public class Addon {

	private int id;
	private String bezeichnung;
	private int preis;
	private int maxBestand;
	private String typ;
	
	public Addon() {
		super();
	}
	
	public Addon(int id, String bezeichnung, int preis, int maxBestand, String typ) {
		super();
		this.id = id;
		this.bezeichnung = bezeichnung;
		this.preis = preis;
		this.maxBestand = maxBestand;
		this.typ = typ;
	}
	
	public void kaufen() {
		
	}
	
	@Override
	public String toString() {
		return "Addon [id=" + id + ", bezeichnung=" + bezeichnung + ", preis=" + preis + ", maxBestand=" + maxBestand + ", typ=" + typ + "]";
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getBezeichnung() {
		return bezeichnung;
	}
	
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	
	public int getPreis() {
		return preis;
	}
	
	public void setPreis(int preis) {
		this.preis = preis;
	}
	
	public int getMaxBestand() {
		return maxBestand;
	}
	
	public void setMaxBestand(int maxBestand) {
		this.maxBestand = maxBestand;
	}
	
	public String getTyp() {
		return typ;
	}
	
	public void setTyp(String typ) {
		this.typ = typ;
	}
	
	
}
