package array;

class ArrayHelper {
	public static String convertArrayToString(int[] zahlen) {
		String ausgabe = "";
		for (int i = 0; i < zahlen.length; i++) {
			ausgabe += zahlen[i];
			if (i < zahlen.length-1) {
				ausgabe += " , ";
			}
		}
		return ausgabe;
	}
}
