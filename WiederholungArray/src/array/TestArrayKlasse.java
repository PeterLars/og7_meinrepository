package array;

import java.util.Scanner;

public class TestArrayKlasse {

	public static void main(String[] args) {
		Scanner eingabe = new Scanner(System.in);

		System.out.println("\nAufgabe 1 1");
		int[] aufgabe1_1_Array = new int[20];
		for (int i = 0; i < aufgabe1_1_Array.length; i++) {
			aufgabe1_1_Array[i] = 1;
		}
		System.out.println(ArrayHelper.convertArrayToString(aufgabe1_1_Array));

		System.out.println("\nAufgabe 1 2");
		int[] aufgabe1_2_Array = new int[15];
		for (int i = 0; i < aufgabe1_2_Array.length; i++) {
			aufgabe1_2_Array[i] = i;
		}
		System.out.println(ArrayHelper.convertArrayToString(aufgabe1_2_Array));

		System.out.println("\nAufgabe 1 3");
		int[] aufgabe1_3_Array = new int[17];
		for (int i = 0; i < aufgabe1_3_Array.length; i++) {
			aufgabe1_3_Array[i] = aufgabe1_3_Array.length - i - 1;
		}
		System.out.println(ArrayHelper.convertArrayToString(aufgabe1_3_Array));

		System.out.println("\nAufgabe 2 1");
		int[] aufgabe2_1_Array = new int[5];
		for (int i = 0; i < aufgabe2_1_Array.length; i++) {
			System.out.print(i + ". Zahl eingeben : ");
			aufgabe2_1_Array[i] = eingabe.nextInt();
		}
		System.out.println(ArrayHelper.convertArrayToString(aufgabe2_1_Array));

		System.out.println("\nAufgabe 2 2");
		System.out.print("Die l�nge des Arrays: ");
		int aufgabe2_2_x = eingabe.nextInt();
		int[] aufgabe2_2_Array = new int[aufgabe2_2_x];
		for (int i = 0; i < aufgabe2_2_Array.length; i++) {
			aufgabe2_2_Array[i] = i;
		}
		for (int i = 0; i < aufgabe2_2_Array.length; i++) {
			System.out.print(aufgabe2_2_Array[i]);
			System.out.print(" , ");
		}

		System.out.println("\nAufgabe 3 1");
		System.out.print("Die l�nge des Arrays: ");
		int aufgabe3_1_x = eingabe.nextInt();
		int[] augfgabe3_1_array = new int[aufgabe3_1_x];
		for (int i = 0; i < augfgabe3_1_array.length; i++) {
			augfgabe3_1_array[i] = i;
		}
		for (int i = 0; i < augfgabe3_1_array.length; i++) {
			System.out.print(augfgabe3_1_array[i]);
			if (i < augfgabe3_1_array.length - 1) {
				System.out.print(" , ");
			}
		}

		System.out.println("\nAufgabe 3 2");
		System.out.print("Die l�nge des Arrays: ");
		int aufgabe3_2_x = eingabe.nextInt();
		int[][] augfgabe3_2_array = new int[aufgabe3_2_x][aufgabe3_2_x];
		for (int x = 0; x < augfgabe3_2_array.length; x++) {
			for (int y = 0; y < augfgabe3_2_array[x].length; y++) {
				augfgabe3_2_array[x][y] = x * y;
			}
		}
		for (int x = 0; x < augfgabe3_2_array.length; x++) {
			for (int y = 0; y < augfgabe3_2_array[x].length; y++) {
				System.out.print(augfgabe3_2_array[x][y]);
				if (y < augfgabe3_2_array[x].length - 1) {
					System.out.print(" , \t");
				}
			}
			System.out.println("");
		}

		System.out.println("\nAufgabe 4");
		int[] aufgabe4_1 = new int[24];
		for (int i = 0; i < aufgabe4_1.length; i++) {
			aufgabe4_1[i] = (int) (Math.random() * 100);
		}
		System.out.println(ArrayHelper.convertArrayToString(aufgabe4_1));

		eingabe.close();
	}

}
