package de.oszimt.starsim2099;

public class Position {
	protected double posX;
	protected double posY;
	
	public Position() {
		super();
	}
	
	public double getPosX() {
		return posX;
	}
	
	public void setPosX(double posX) {
		this.posX = posX;
	}
	
	public double getPosY() {
		return posY;
	}
	
	public void setPosY(double posY) {
		this.posY = posY;
	}
	
}
