import java.util.ArrayList;
import java.util.List;

public class Logic {
	
	List<String> datei;
	
	public Logic(String dateiname) {
		super();
		reloadFromDisk(dateiname);
	}

	public void reloadFromDisk(String dateiname) {
		datei = FileControl.readFile(dateiname);
	}

	public List<Lerneintrag> getListEntries() {
		List<Lerneintrag> entries = new ArrayList<Lerneintrag>();
		for (int i = 1; i < datei.size()-2; i += 4) {
			try {
				entries.add(new Lerneintrag(datei.get(i), datei.get(i+1), datei.get(i+2), Integer.parseInt(datei.get(i+3))));
			} catch (Exception e) {
				System.err.println("Der Eintrag von Zeile " + (i+1) + " bis Zeile " + (i+4) + " ist Fehlerhaft!");
			}
			
		}
		return entries;
	}

	public String getLearnerName() {
		return datei.get(0);
	}
}
