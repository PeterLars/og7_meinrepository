import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FileControl {

	public static List<String> readFile(String fileName) {
		List<String> listOfLines = new ArrayList<>();
		try {
			Scanner myScanner = new Scanner(new FileInputStream(fileName), StandardCharsets.UTF_8.name());
			while (myScanner.hasNextLine()) {
				listOfLines.add(myScanner.nextLine());
			}
			myScanner.close();
		} catch (FileNotFoundException e) {
			System.err.println(e.getMessage());
		}
		return listOfLines;
	}

	public static boolean writeFile(List<String> file, String fileName) {
		BufferedWriter out = null;

		try {
			out = new BufferedWriter(new FileWriter(fileName));
			for (int i = 0; i < file.size(); i++) {
				out.write(file.get(i)+ "\n");
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (out != null) {
					out.close();
				} else {
					System.out.println("Buffer has not been initialized!");
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return false;
	}

}
