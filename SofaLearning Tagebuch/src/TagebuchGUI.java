
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.ListSelectionModel;
import javax.swing.JScrollPane;


public class TagebuchGUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable table;
	private Logic logic = new Logic("lars.dat");

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TagebuchGUI frame = new TagebuchGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TagebuchGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 563, 480);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblLerntagebuchVon = new JLabel("Lerntagebuch von " + logic.getLearnerName());
		lblLerntagebuchVon.setBounds(12, 13, 699, 29);
		lblLerntagebuchVon.setFont(new Font("Tahoma", Font.PLAIN, 22));
		contentPane.add(lblLerntagebuchVon);

		JButton btnBeenden = new JButton("Beenden");
		btnBeenden.setBounds(414, 369, 97, 25);
		btnBeenden.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		contentPane.add(btnBeenden);

		JButton btnBericht = new JButton("Bericht");
		btnBericht.setBounds(305, 369, 97, 25);
		btnBericht.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

			}
		});
		contentPane.add(btnBericht);

		JButton btnNeuereintrag = new JButton("Neuer Eintrag");
		btnNeuereintrag.setBounds(182, 369, 111, 25);
		btnNeuereintrag.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				List<String> file = FileControl.readFile("lars.dat");
				file.add("Hallo das ist ein Test");
				FileControl.writeFile(file, "lars.dat");
			}
		});
		contentPane.add(btnNeuereintrag);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(22, 59, 465, 252);
		contentPane.add(scrollPane);
		table = new JTable();
		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("Datum");
		model.addColumn("Fach");
		model.addColumn("Beschreibung");
		model.addColumn("Dauer");
		List<Lerneintrag> entries = logic.getListEntries();
		for (int i = 0; i < entries.size(); i++) {
			model.addRow(new Object[] { entries.get(i).getDatum(), entries.get(i).getFach(),
					entries.get(i).getBeschreibung(), entries.get(i).getDauer() });
		}
		table.setModel(model);
		table.setFillsViewportHeight(true);
		scrollPane.setViewportView(table);
		table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
	}
}
