
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Lerneintrag {
	private Date datum;
	private String fach;
	private String beschreibung;
	private int dauer;

	public Lerneintrag(String datum, String fach, String beschreibung, int dauer) {
		super();
		this.setDatum(datum);
		this.setFach(fach);
		this.setBeschreibung(beschreibung);
		this.setDauer(dauer);
	}

	public String getDatum() {
		SimpleDateFormat sdfToDate = new SimpleDateFormat("dd.MM.yyyy");
		return sdfToDate.format(this.datum);
	}
	
	public void setDatum(String datum) {
		try {
			SimpleDateFormat sdfToDate = new SimpleDateFormat("dd.MM.yyyy");
			this.datum = sdfToDate.parse(datum);
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}

	public String getFach() {
		return fach;
	}

	public void setFach(String fach) {
		this.fach = fach;
	}

	public String getBeschreibung() {
		return beschreibung;
	}

	public void setBeschreibung(String beschreibung) {
		this.beschreibung = beschreibung;
	}

	public int getDauer() {
		return dauer;
	}

	public void setDauer(int dauer) {
		this.dauer = dauer;
	}

}
