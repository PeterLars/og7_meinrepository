package arrayList;

import java.util.ArrayList;
import java.util.List;

public class Telefonbuch {

	
	public static final int NOT_FOUND = -1;
	List<TelefonbuchEintrag> telefonbuch = new ArrayList<TelefonbuchEintrag>();

	//public boolean addEintrag() {
	//}
	
	public static int indexOf(List<TelefonbuchEintrag> telefonbuch, String nachName) {
		for (int i = 0; i < telefonbuch.size(); i++) {
			if (telefonbuch.get(i).getPerson().getNachName().equals(nachName)) {
				return i;
			}
		}
		return NOT_FOUND;
	}

}
