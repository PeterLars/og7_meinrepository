package arrayList;

public class Person{
	//Attribute
	private String vorName;
	private String nachName;
	private int alter;

	//Constructor
	public Person() {
		super();
	}

	public Person(String vorName, String nachName, int alter) {
		super();
		this.alter = alter;
		this.vorName = vorName;
		this.nachName = nachName;
	}

	//toSting
	@Override
	public String toString() {
		return "Person [vorName=" + vorName + ", nachName=" + nachName + ", alter=" + alter + "]";
	}	
	
	//Getter und Setter
	public String getVorName() {
		return vorName;
	}

	public void setVorName(String vorName) {
		this.vorName = vorName;
	}

	public String getNachName() {
		return nachName;
	}

	public void setNachName(String nachName) {
		this.nachName = nachName;
	}

	public int getAlter() {
		return alter;
	}

	public void setAlter(int alter) {
		this.alter = alter;
	}


}
