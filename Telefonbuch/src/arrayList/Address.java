package arrayList;

public class Address {
	// Attribute
	private String strasse;
	private String hausnummer;
	private String plz;
	private String stadt;
	private String ort;

	// Constructor
	public Address() {
		super();
	}

	public Address(String strasse, String hausnummer, String plz, String stadt, String ort) {
		super();
		this.strasse = strasse;
		this.hausnummer = hausnummer;
		this.ort = ort;
		this.plz = plz;
		this.stadt = stadt;
	}

	//toSting
	@Override
	public String toString() {
		return "Address [strasse=" + strasse + ", hausnummer=" + hausnummer + ", plz=" + plz + ", stadt=" + stadt
				+ ", ort=" + ort + "]";
	}

	// Getter und Setter
	public String getStrasse() {
		return strasse;
	}

	public void setStrasse(String strasse) {
		this.strasse = strasse;
	}

	public String getHausnummer() {
		return hausnummer;
	}

	public void setHausnummer(String hausnummer) {
		this.hausnummer = hausnummer;
	}

	public String getOrt() {
		return ort;
	}

	public void setOrt(String ort) {
		this.ort = ort;
	}

	public String getPlz() {
		return plz;
	}

	public void setPlz(String plz) {
		this.plz = plz;
	}

	public String getStadt() {
		return stadt;
	}

	public void setStadt(String stadt) {
		this.stadt = stadt;
	}
}
