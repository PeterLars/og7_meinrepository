package arrayList;

public class TelefonbuchEintrag  implements Comparable<TelefonbuchEintrag>{
	// Attribute
	private Person person;
	private Address adresse;
	private String telefonNummer; 

	// Constructor
	public TelefonbuchEintrag() {
		super();
	}
	
	public TelefonbuchEintrag(Person person, Address adresse, String telefonNummer) {
		super();
		this.person = person;
		this.adresse = adresse;
		this.telefonNummer = telefonNummer;
	}

	//toString
	@Override
	public String toString() {
		return "TelefonbuchEintrag [person=" + person + ", adresse=" + adresse + ", telefonNummer=" + telefonNummer
				+ "]";
	}

	@Override
	public boolean equals(Object o) {
		return this.getPerson().getNachName().equals(((Person) o).getNachName());
	}

	@Override
	public int compareTo(TelefonbuchEintrag aTelefonbuchEintrag) {
		return this.getPerson().getNachName().compareTo(aTelefonbuchEintrag.getPerson().getNachName());
	}
	
	//Getter und Setter
	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public Address getAdresse() {
		return adresse;
	}

	public void setAdresse(Address adresse) {
		this.adresse = adresse;
	}

	public String getTelefonNummer() {
		return telefonNummer;
	}

	public void setTelefonNummer(String telefonNummer) {
		this.telefonNummer = telefonNummer;
	}
	
}
