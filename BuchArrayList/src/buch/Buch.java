package buch;

public class Buch implements Comparable<Buch> {
	private String autor;
	private String titel;
	private String isbn;

	public Buch(String autor, String titel, String isbn) {
		this.setAutor(autor);
		this.setIsbn(isbn);
		this.setTitel(titel);
	}

	public Buch() {
		super();
		this.setAutor("");
		this.setIsbn("");
		this.setTitel("");
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getTitel() {
		return titel;
	}

	public void setTitel(String titel) {
		this.titel = titel;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	@Override
	public String toString() {
		return "Buch [autor=" + autor + ", titel=" + titel + ", isbn=" + isbn + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((isbn == null) ? 0 : isbn.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Buch other = (Buch) obj;
		if (isbn == null) {
			if (other.isbn != null)
				return false;
		} else if (!isbn.equals(other.isbn))
			return false;
		return true;
	}

	@Override
	public int compareTo(Buch aBuch) {
		return this.getIsbn().compareTo(aBuch.getIsbn());
	}

}