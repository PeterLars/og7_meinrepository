package buch;

import java.util.*;

public class CollectionTest {

	static void menue() {
		System.out.println("\n ***** Buch-Verwaltung *******");
		System.out.println(" 1) eintragen ");
		System.out.println(" 2) finden ");
		System.out.println(" 3) löschen");
		System.out.println(" 4) Die größte ISBN");
		System.out.println(" 5) zeigen");
		System.out.println(" 9) Beenden");
		System.out.println(" ********************");
		System.out.print(" Bitte die Auswahl treffen: ");
	} // menue

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		List<Buch> buchliste = new ArrayList<Buch>();

		Scanner myScanner = new Scanner(System.in);

		char wahl;
		String eintrag;

		do {
			menue();
			wahl = myScanner.next().charAt(0);
			switch (wahl) {
			case '1':

				myScanner.nextLine();

				System.out.print("Autor: ");
				String autor = myScanner.nextLine();

				System.out.print("Titel: ");
				String titel = myScanner.nextLine();

				System.out.print("ISBN-10:");
				String isbn = "ISBN-10:" + myScanner.nextLine();

				buchliste.add(new Buch(autor, titel, isbn));
				break;
			case '2':
				System.out.print("ISBN-10:");
				eintrag = "ISBN-10:" + myScanner.next();
				System.out.println(findeBuch(buchliste, eintrag));
				break;
			case '3':
				System.out.print("ISBN-10:");
				eintrag = "ISBN-10:" + myScanner.next();
				loescheBuch(buchliste, new Buch("", "", eintrag));
				break;
			case '4':
				System.out.println(ermitteleGroessteISBN(buchliste));
				break;
			case '5':
				for (int i = 0; i < buchliste.size(); i++) {
					System.out.println(buchliste.get(i));
				}
				break;
			case '9':
				System.exit(0);
				break;
			case 'l':
				int anzahl = myScanner.nextInt();
				for (int i = 0; i < anzahl; i++) {
					buchliste.add(new Buch((int) (Math.random()*10000)+ "", (int) (Math.random()*10000) + "", (int) (Math.random()*10000)+""));
				}
				break;
			default:
				menue();
				wahl = myScanner.next().charAt(0);
			} // switch

		} while (wahl != 9);
		myScanner.close();
	}// main

	public static void menue2() {
		System.out.println();
		System.out.println("************ Buch-Verwaltung ************");
		System.out.println("Über welches Attribut möchten sie suchen?");
		System.out.println("1) Autor");
		System.out.println("2) Titel");
		System.out.println("3) ISBN");
		System.out.println("*****************************************");
		System.out.print(" Bitte die Auswahl treffen: ");
	}

	public static Buch findeBuch(List<Buch> buchliste, String isbn) {
		for (int index = 0; index < buchliste.size(); index++) {
			if (buchliste.get(index).equals(new Buch("", "", isbn)))
				return buchliste.get(index);
		}
		return null;
	}

	public static boolean loescheBuch(List<Buch> buchliste, Buch aBuch) {
		return buchliste.remove(aBuch);
	}

	public static String ermitteleGroessteISBN(List<Buch> buchliste) {
		Buch aBuch = new Buch();
		for (int i = 0; i < buchliste.size(); i++) {
			if (buchliste.get(i).compareTo(aBuch) > 0) {
				aBuch = buchliste.get(i);
			}
		}
		return aBuch.getIsbn();
	}

}